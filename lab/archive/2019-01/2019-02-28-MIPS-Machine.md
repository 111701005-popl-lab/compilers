# Support for MIPS architecture.


Implement a module for capturing

1. The MIPS assembly language instructions parameterised on labels and
   temps.

2. The registers of MIPS

3. Functions to convert the labels, temps, and instructions
   to MIPS assembly language strings (pretty printing).


This will be used in the subsequent assignments for things like
instruction selection.

You can assume that you are working with [SPIM] in which case
the follow <http://www.dsi.unive.it/~arcb/LAB/spim.htm> as your
quick guide to MIPS assembly.

## The `Temp` structure

To manage the generation of temporary variables and labels you should
start by coding up your `Temp` structure with the following signature

```
signature TEMP = sig
	type temp  (* temporary variables of your program *)
	type label (* temporary labels for your program *)
	val newlabel : unit -> label
	val newtemp  : unit -> temp

end
```

Ensure that your MIPS instructions all take `temp` for variables and
`label` for jump locations.

[SPIM]: <http://www.dsi.unive.it/~arcb/LAB/spim.htm>
